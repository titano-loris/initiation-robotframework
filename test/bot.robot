*** Settings ***
Library    SeleniumLibrary
Resource    ../ressouces/doc.resource
*** Variables ***
${url}    https://petstore.octoperf.com/actions/Catalog.action
${browser}    chrome

*** Keywords ***
Ouvrir browser
    Open Browser    ${url}    ${browser} 
Page Login
    Click Element    locator=//a[normalize-space()='Sign In']
    Clear Element Text    locator=//input[@name='password']  

*** Test Cases ***
Ouvrir jpetstore
    Ouvrir browser
    Page Login
   